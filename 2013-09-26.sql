-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 26, 2013 at 12:28 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `php_db`
--
CREATE DATABASE IF NOT EXISTS `php_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `php_db`;

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `picture` varchar(45) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `level_id` int(1) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `username`, `password`, `email`, `fullname`, `picture`, `status`, `level_id`, `created`, `updated`, `deleted`) VALUES
(17, 'post', 'ppp222', 'jpostslj@hojtma.ocm', 'fullanme', '2013-09-21-23-48-39.png', 1, 1, '2013-09-21 23:30:55', '2013-09-21 23:57:51', NULL),
(20, 'member', '1234', 'aaa@hotmaol.com', 'ewwww', NULL, 2, 1, '2013-09-21 23:55:17', '2013-09-21 23:57:44', '2013-09-27 00:02:35'),
(24, 'admin', '1234', 'post@hotmail.com', 'aaaa', '2013-10-03-22-13-12.jpg', 1, 1, '2013-09-22 00:04:54', '2013-10-03 23:23:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `summary` varchar(256) DEFAULT NULL,
  `content` text NOT NULL,
  `status` int(1) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime DEFAULT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `name`, `summary`, `content`, `status`, `picture`, `created`, `updated`, `deleted`, `account_id`) VALUES
(1, 'หลุดผลทดสอบความแรง Nexus 5 แรงสูสี iPhone 5S!', 'ประมาณกลางเดือนตุลาคม อีกไม่นานนี้แฟนสมาร์ทโฟนตระกูล Nexus เตรียมตัวรอดูมือถือรุ่นใหม่ได้เลยครับ คือ Google Nexus 5 สมาร์ทโฟน Pure Android ที่รังสรรค์โดย Google และ LG เจ้าเก่า ผู้ผลิต Nexus 4 ให้ดัง และเป็นที่นืยมมากในกลุ่มผู้ใช้ Android ที่เสพติดความลื่น', 'ประมาณกลางเดือนตุลาคม อีกไม่นานนี้แฟนสมาร์ทโฟนตระกูล Nexus เตรียมตัวรอดูมือถือรุ่นใหม่ได้เลยครับ คือ Google Nexus 5 สมาร์ทโฟน Pure Android ที่รังสรรค์โดย Google และ LG เจ้าเก่า ผู้ผลิต Nexus 4 ให้ดัง และเป็นที่นืยมมากในกลุ่มผู้ใช้ Android ที่เสพติดความลื่นไหล\r\n', 1, '', '2013-09-30 07:08:10', '2013-09-30 06:10:05', NULL, 24),
(7, 'Hello World', '<p>Hello World</p>', '<p style="text-align: center;"><strong>Hello World</strong></p>', 1, '2013-10-06-21-57-45.jpg', '2013-10-06 21:51:21', '2013-10-06 21:57:45', NULL, 24);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
