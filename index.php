<?php require('inc_header.php'); ?>

  <body>

  <?php require('inc_navigation.php');?>

    <!-- Docs page layout -->
    <!-- 
    <div class="bs-header" id="content">
      <div class="container">
        <h1>Getting started</h1>
        <p>An overview of Bootstrap, how to download and use, basic templates and examples, and more.</p>
        <div id="carbonads-container"><div class="carbonad"><div id="azcarbon"></div><script>var z = document.createElement("script"); z.async = true; z.src = "http://engine.carbonads.com/z/32341/azcarbon_2_1_0_HORIZ"; var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(z, s);</script></div></div>

      </div>
    </div>
    -->


    <div class="container bs-docs-container">

      <div class="row">
        <div class="col-md-3">
          <div class="bs-sidebar hidden-print" role="complementary">

          <?php require('inc_sidebar.php'); ?>

          </div>
        </div>

        <div class="col-md-9" role="main">
        <!-- Getting started
        ================================================== -->
        <?php

        $sql  = "SELECT * FROM posts ";
        $query = mysql_query($sql);

        $Num_Rows = mysql_num_rows($query);

        // โค๊ดแบ่งหน้า
        $Per_Page = 10;   // per page

        $Page = (isset($_GET["Page"]) ? $_GET['Page'] : 1);

        $Prev_Page = $Page - 1;
        $Next_Page = $Page + 1;

        $Page_Start = (($Per_Page * $Page) - $Per_Page);

        if ($Num_Rows <= $Per_Page) {
            $Num_Pages = 1;
        } else if (($Num_Rows % $Per_Page) == 0) {
            $Num_Pages = ($Num_Rows / $Per_Page);
        } else {
            $Num_Pages = ($Num_Rows / $Per_Page) + 1;
            $Num_Pages = (int) $Num_Pages;
        }

        $sql .= " ORDER BY id ASC LIMIT $Page_Start , $Per_Page";
        $query = mysql_query($sql);

        if (mysql_num_rows($query) > 0) {
            // วนลูปข้อมูล
            while ($item = mysql_fetch_array($query)) {
                    ?>
        <div class="row">
          <div class="bs-docs-section">
              <div class="page-header">
                <h1 id="download-bootstrap-<?=$i;?>"><?php echo $item['name']; ?></h1>
              </div>
              <p class="lead">
                <img src="http://placehold.it/200x150" style="margin:10px;float:left;">
                <?php echo $item['content']; ?>
              </p>
              <p><span class="glyphicon glyphicon-time"></span> โพสต์เมื่อ: <?php echo date('Y-m-d H:i', strtotime($item['created'])); ?> <span class="pull-right"><a href="article_detail.php?id=<?php echo $item['id'];?>" class="btn">อ่านต่อ</a></span></p>
            
          </div>
        </div>
                  <?php
          }
      }
      ?>
        </div>


        </div>
      </div>

    <!-- Footer
    ================================================== -->
    <footer class="bs-footer" role="contentinfo">
      <div class="container">
        <div class="bs-social">
  <ul class="bs-social-buttons">
    <li>
      <iframe class="github-btn" src="http://ghbtns.com/github-btn.html?user=twbs&amp;repo=bootstrap&amp;type=watch&amp;count=true" width="100" height="20" title="Star on GitHub"></iframe>
    </li>
    <li>
      <iframe class="github-btn" src="http://ghbtns.com/github-btn.html?user=twbs&amp;repo=bootstrap&amp;type=fork&amp;count=true" width="102" height="20" title="Fork on GitHub"></iframe>
    </li>
    <li class="follow-btn">
      <a href="https://twitter.com/twbootstrap" class="twitter-follow-button" data-link-color="#0069D6" data-show-count="true">Follow @twbootstrap</a>
    </li>
    <li class="tweet-btn">
      <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://getbootstrap.com/" data-count="horizontal" data-via="twbootstrap" data-related="mdo:Creator of Twitter Bootstrap">Tweet</a>
    </li>
  </ul>
</div>


        <p>Designed and built with all the love in the world by <a href="http://twitter.com/mdo" target="_blank">@mdo</a> and <a href="http://twitter.com/fat" target="_blank">@fat</a>.</p>
        <p>Code licensed under <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache License v2.0</a>, documentation under <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>.</p>
        <ul class="footer-links">
          <li><a href="../2.3.2/">Bootstrap 2.3.2 docs</a></li>
          <li class="muted">&middot;</li>
          <li><a href="http://blog.getbootstrap.com">Blog</a></li>
          <li class="muted">&middot;</li>
          <li><a href="https://github.com/twbs/bootstrap/issues?state=open">Issues</a></li>
          <li class="muted">&middot;</li>
          <li><a href="https://github.com/twbs/bootstrap/releases">Releases</a></li>
        </ul>
      </div>
    </footer>

<?php require('inc_footer.php'); ?>