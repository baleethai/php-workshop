<?php require('inc_header.php'); ?>
<body>

    <?php require('inc_navigation.php'); ?>

    <br><br>
    <div class="row well">

                <form action="">
                <a href="article_add.php" class="btn btn-default"> <span class="glyphicon glyphicon-plus"></span> เพิ่มโพสต์</a>
                    <span class="pull-right"><input type="text" name="s" class="form-control" placeholder="ค้นหา"></span>
                </form>
                <br>
                <table class="table table-striped">
                    <thead>
                        <tr class="success">
                            <th>ID</th>
                            <th>ชื่อเรื่อง</th>
                            <th>ผู้เขียน</th>
                            <th>เขียนเมื่อ</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // เช็คว่าถ้ายังไม่ได้ล๊อกอินมา ให้กลับไปล๊อกอินก่อน(sign_in.php)
                        if( !isset($_SESSION['logged']) && $_SESSION['logged'] == '' )
                        {
                            header('location:sign_in.php');
                        }
                        // query ข้อมูลออกมาจากฐานข้อมูล เทเบิล posts,accounts โดยสถานะต้องเท่ากับ 1 คือเปิดใช้งาน และฟิวต์ deleted ต้องเท่ากับ NULL คือไม่มีค่าอะไร
                        $sql  = "SELECT *,posts.id as post_id,posts.status as post_status FROM posts LEFT JOIN accounts ON posts.account_id = accounts.id AND posts.status = 1 AND posts.deleted IS NULL ";
                        // search 
                        if( isset($_GET['s']) && $_GET['s'] != '' )
                        {
                            $sql .= "AND (`name` LIKE '".$_GET['s']."') ";
                        }
                        echo $sql;
                        $query = mysql_query($sql);
                        // นับจำนวนที่ query ออกมาได้
                        $Num_Rows = mysql_num_rows($query);

                        // จะแบ่งกี่หน้า
                        $Per_Page = 10;
                        // ถ้าไม่มีตัวแปร $Page ก็ให้เท่ากับ 1
                        $Page = (isset($_GET["Page"]) ? $_GET['Page'] : 1);

                        $Prev_Page = $Page - 1;
                        $Next_Page = $Page + 1;

                        $Page_Start = (($Per_Page * $Page) - $Per_Page);

                        if ($Num_Rows <= $Per_Page) {
                            $Num_Pages = 1;
                        } else if (($Num_Rows % $Per_Page) == 0) {
                            $Num_Pages = ($Num_Rows / $Per_Page);
                        } else {
                            $Num_Pages = ($Num_Rows / $Per_Page) + 1;
                            $Num_Pages = (int) $Num_Pages;
                        }

                        $sql .= " ORDER BY posts.id ASC LIMIT $Page_Start , $Per_Page";
                        $query = mysql_query($sql);

                        if (mysql_num_rows($query) > 0) {
                            // วนลูปข้อมูลที่ query ได้และแบ่งหน้าเรียบร้อย
                            while ($item = mysql_fetch_array($query)) {

                                    ?>
                                    <tr>
                                        <td><?php echo $item['post_id']; ?>.</td>
                                        <td><?php echo $item['name']; ?></td>
                                        <td><?php echo $item['username']; ?></td>
                                        <td><?php if( $item['post_status'] == 1 ){echo 'เปิดใช้งาน';}else{echo 'ปิดใช้งาน';} ?></td>
                                        <td>
                                            <?php
                                            echo date("Y", strtotime($item['created'])) + 543;
                                            echo "-";
                                            echo date("m-d", strtotime($item['created']));
                                            ?>
                                        </td>
                                        <td>

                                            <a href="article_edit.php?id=<?php echo $item['post_id']; ?>"><span class="glyphicon glyphicon-pencil"></span>
                                                แก้ไข
                                            </a> | 

                                            <a href="article_delete.php?id=<?php echo $item['post_id']; ?>" onclick="return confirm('ต้องการจะลบ !');"><span class="glyphicon glyphicon-trash"></span>
                                                ลบ
                                            </a>

                                        </td>
                                    </tr>
                                    <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>

                <div class="panel-footer">

                    <ul class="pagination">
                        <!-- แสดงการแบ่งหน้า -->
                        <?php if ($Prev_Page): ?>
                            <li><a href="member_view.php?Page=<?= $Prev_Page; ?>">&laquo;</a></a></li>
                        <?php endif; ?>

                        <?php
                        for ($i = 1; $i <= $Num_Pages; $i++) {
                            if ($i != $Page) {
                                echo "<li><a href='$_SERVER[SCRIPT_NAME]?Page=$i'>$i</a> </li>";
                            } else {
                                echo "<li><a href='#'>$i</a></li>";
                            }
                        }
                        ?>

                            <?php if ($Page != $Num_Pages): ?>
                            <li><a href="member_view.php?Page=<?= $Next_Page; ?>">&raquo;</a>
                        <?php endif; ?>

                    </ul>

                </div>

    </div>

</body>

<?php require('inc_footer.php'); ?>
