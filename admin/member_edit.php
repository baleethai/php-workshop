<?php require('inc_header.php'); ?>

<body>

    <?php require('inc_navigation.php'); ?>

    <div class="container">
        <div class="row">

            <br><br>
            <h1><span class="glyphicon glyphicon-user"></span>
                แก้ไขสมาชิก: <?php echo (isset($_COOKIE['id'])?$_COOKIE['id']:''); ?>
            </h1>
            <?php
            //delete picture
            if (isset($_GET['pdel']) && $_GET['pdel'] == 1) {
                mysql_query("UPDATE `php_db`.`accounts` SET `picture` = '' WHERE `accounts`.`id` = $_GET[id] ");
                unlink('uploads/' . $_GET['path']);
                // redirect to member_edit.php
                echo '<script type="text/javascript">
                        window.location="member_edit.php?id=$_GET[id]";
                    </script>;';
            }

            // edit
            if ($_POST) {

                // validation
                if ( $_POST['username'] == '' || $_POST['password'] == '' || $_POST['email'] == '') {
                    ?>
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        กรุณากรอกข้อูลให้ครบ !
                    </div>
        <?php
    } else {
        
        $date_now = date("Y-m-d H:i:s");
        $result = mysql_query("UPDATE `php_db`.`accounts` SET `username` = '$_POST[username]', `password` = '$_POST[password]', `email` = '$_POST[email]', `fullname` = '$_POST[fullname]', `updated` = '$date_now' WHERE `accounts`.`id` = $_POST[id];
							");

        if (isset($_FILES['picture']['name'])) {

            $file_type = $_FILES['picture']['type'];
            // check type picture
            if ($file_type == 'image/jpeg' || $file_type == 'image/png' || $file_type == 'image/gif') {
                // rename 
                $Str_file = explode(".", $_FILES['picture']['name']);  // แยกชื่อไฟล์ด้วย "." ค่าที่ได้จะเป็น Array ครับ
                $date = date('Y-m-d-H-i-s');
                $new_name = $date . "." . $Str_file['1'];
                // upload
                if (move_uploaded_file($_FILES["picture"]["tmp_name"], "uploads/" . $new_name)) {
                    mysql_query("UPDATE `php_db`.`accounts` SET `picture` = '$new_name' WHERE `accounts`.`id` = $_POST[id] ");
                }
            }
        }
        
        if (isset($result) && $result == 1) {
            // redirect to member_edit.php
            echo '<script type="text/javascript">
                    window.location="member_view.php";
                </script>;';
        }
    }
            }
        ?>


            <form action="member_edit.php" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
            
                <?php
            $id = isset($_GET['id']) ? $_GET['id'] : "";
            $query = mysql_query("SELECT * FROM `accounts` WHERE id =$id") or die(mysql_errno());
            while ($item = mysql_fetch_array($query)) {
                ?>
                    <div class="form-group">
                        <label for="inputUsername" class="col-lg-2 control-label">ชื่อเข้าสู่ระบบ</label>
                        <div class="col-lg-10">
                            <input name="username" type="username" class="form-control" placeholder="Username" value="<?php echo $item['username']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputPassword1" class="col-lg-2 control-label">รหัสผ่าน</label>
                        <div class="col-lg-10">
                            <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo $item['password']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">อีเมล์</label>
                        <div class="col-lg-10">
                            <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $item['email']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputFullname" class="col-lg-2 control-label">ชื่อ-นามสุลเต็ม</label>
                        <div class="col-lg-10">
                            <input type="fullname" name="fullname" class="form-control" placeholder="Fullname" value="<?php echo $item['fullname']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputEmail1" class="col-lg-2 control-label">รูปภาพ</label>
                        <div class="col-lg-10">
                            <?php if (isset($item['picture']) && $item['picture'] != '' && is_file('uploads/'.$item['picture'])) { ?>
                                <img src="uploads/<?php echo $item['picture']; ?>" width="100" height="100">
                                <p><a href="member_edit.php?pdel=1&path=<?php echo $item['picture']; ?>&id=<?php echo $item['id']; ?>" class="btn btn-link">ลบ</a></p>
                            <?php } else { ?>

                                <input type="file" name="picture">

                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputStatus" class="col-lg-2 control-label">สถานะ</label>
                        <div class="col-lg-10">
                            <select name="status" class="form-control">
                                <option value="1">เปิดใช้งาน</option>
                                <option value="0">ปิดใช้งาน</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputLevel_id" class="col-lg-2 control-label">ระดับ</label>
                        <div class="col-lg-10">
                            <select name="level_id" class="form-control">
                                <option value="1">Admin</option>
                                <option value="2">Member</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-10">
                            <button type="submit" class="btn btn-success">Sign Up</button>
                        </div>
                    </div>

                    <input type="hidden" name="id" value="<?php echo $item['id']; ?>">

<?php } ?>
            </form>


        </div>
    </div>

<?php require('inc_footer.php'); ?>
