<?php require('inc_header.php'); ?>
<body>

    <?php require('inc_navigation.php'); ?>

        <div class="row well">

            <div class="span10">
            <br><br>
            <h1><span class="glyphicon glyphicon-pencil"></span>
                เพิ่มโพสต์
            </h1>
            <?php
            // เช็คว่ามีการล๊อกอินมรหรือไม่ ?
            if( !isset($_SESSION['logged']) && $_SESSION['logged'] == '' )
            {
                header('location:sign_in.php');
            }

            // เมื่อมีการกด submit 
            if ($_POST) {
                
                // validation
                if ( $_POST['name'] == ''  ||  $_POST['content'] == '' ) {
                    ?>
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        กรุณากรอกข้อูลให้ครบ !
                    </div>
                    <?php
                    
                } else {
                    // ประกาศตัวแปร date_now โดยเก็บวันที่ปัจจุบัน
                    $date_now = date("Y-m-d H:i:s");
                    
                    $result = mysql_query("UPDATE `php_db`.`posts` SET `name` = '$_POST[name]', `summary` = '$_POST[summary]', `content` = '$_POST[content]', `status` = '$_POST[status]', `updated` = '$date_now' WHERE `posts`.`id` = '$_POST[id]' ");
                    
                    if (isset($_FILES['picture']['name'])) {
                        
                        $file_type = $_FILES['picture']['type'];
                        // check type picture
                        if ( $file_type == 'image/jpeg' || $file_type == 'image/png' || $file_type == 'image/gif') {
                            // rename 
                            $Str_file = explode(".", $_FILES['picture']['name']);  // แยกชื่อไฟล์ด้วย "." ค่าที่ได้จะเป็น Array ครับ
                            $date = date('Y-m-d-H-i-s');
                            $new_name = $date . "." . $Str_file['1'];
                            // move image to "uploads" folder
                            if (move_uploaded_file($_FILES["picture"]["tmp_name"], "uploads/" . $new_name)) {
                                mysql_query("UPDATE `php_db`.`posts` SET `picture` = '$new_name' WHERE id = '$_POST[id]' ");
                            }
                        }
                    }

                    // redirect to 'article_view.php'
                    if ( isset($result) && $result == 1) {
                        echo '<script type="text/javascript">
                                    window.location="article_view.php";
                                </script>;';
                    }

                }
                ?>


                <?php
            }
            ?>

            <!-- Start FORM Article -->
            <form action="article_edit.php" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
                <?php
                // SELECT ข้อมูล
                $sql = mysql_query("SELECT * FROM posts WHERE id = '$_GET[id]' "); 
                $num = mysql_num_rows($sql);
                if( $num > 0 ){
                    while ($item = mysql_fetch_array($sql)) { 
                ?>
                <div class="form-group">
                    <label for="inputName" class="col-lg-2 control-label">ชื่อเรื่อง</label>
                    <div class="col-lg-10">
                        <input name="name" type="text" class="form-control" value="<?php echo $item['name'];?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputSummary" class="col-lg-2 control-label">เรื่องย่อ</label>
                    <div class="col-lg-10">
                        <textarea name="summary"><?php echo $item['summary']; ?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputContent" class="col-lg-2 control-label">รายละเอียด</label>
                    <div class="col-lg-10">
                        <textarea name="content" rows="10"><?php echo $item['content']; ?></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputPicture" class="col-lg-2 control-label">ภาพปก</label>
                    <div class="col-lg-10">
                        <?php if( is_file('uploads/'.$item['picture'])){ ?>
                        <img src="uploads/<?php echo $item['picture']; ?>" width="250" heigh="100">
                        <?php } ?>
                        <input type="file" name="picture">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputStatus" class="col-lg-2 control-label">สถานะ</label>
                    <div class="col-lg-10">
                        <select name="status" class="form-control">
                            <option value="1" <?php if($item['status']==1){?>selected<?php } ?>>เปิดใช้งาน</option>
                            <option value="0" <?php if($item['status']==0){?>selected<?php } ?>>ปิดใช้งาน</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                        <input type="hidden" name="id" value="<?php echo $item['id'];?>">
                    </div>
                </div> 
                <?php
                    }
                }
                ?>

            </form>
            <!-- End FORM Article -->
        </div>

        </div>

<?php require('inc_footer.php'); ?>
