<?php require('inc_header.php'); ?>

<body>

    <?php require('inc_navigation.php'); ?>


        <div class="row well">

            <div class="col-md-12">

            <br><br>
            <h1><span class="glyphicon glyphicon-user"></span>
                เพิ่มสมาชิก
            </h1>
            <?php
            
            if ($_POST) {
                
                
                // validation
                if ( $_POST['username'] == ''  ||  $_POST['password'] == '' || $_POST['email'] == '' ) {
                    ?>
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        กรุณากรอกข้อูลให้ครบ !
                    </div>
                    <?php
                    
                } else {
                    // save
                    $date_now = date("Y-m-d H:i:s");
                    
                    $result = mysql_query("INSERT INTO `php_db`.`accounts` (`id`, `username`, `password`, `email`, `fullname`, `picture`, `status`, `level_id`, `created`, `updated`, `deleted`) VALUES (NULL, '$_POST[username]', '$_POST[password]', '$_POST[email]', '$_POST[fullname]', NULL, '$_POST[status]', '$_POST[level_id]', '$date_now', NULL, NULL);");
                    
                    $last_id = mysql_insert_id();
                    
                    if (isset($_FILES['picture']['name'])) {
                        
                        $file_type = $_FILES['picture']['type'];
                        // check type picture
                        if ( $file_type == 'image/jpeg' || $file_type == 'image/png' || $file_type == 'image/gif') {
                            // rename 
                            $Str_file = explode(".", $_FILES['picture']['name']);  // แยกชื่อไฟล์ด้วย "." ค่าที่ได้จะเป็น Array ครับ
                            $date = date('Y-m-d-H-i-s');
                            $new_name = $date . "." . $Str_file['1'];
                            // upload
                            if (move_uploaded_file($_FILES["picture"]["tmp_name"], "uploads/" . $new_name)) {
                                //echo "<p style='color:green;'>อัพโหลดสำเร็จ $new_name</p>";
                                mysql_query("UPDATE `php_db`.`accounts` SET `picture` = '$new_name' WHERE `accounts`.`id` = $last_id ");
                            }
                        }
                    }
                    
                    if ( isset($result) && $result == 1) {
                            echo '<script type="text/javascript">
                                    window.location="member_view.php";
                                </script>;';
                    }
                    
                }
                ?>


                <?php
            }
            ?>


            <form action="member_add.php" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="inputUsername" class="col-lg-2 control-label">ชื่อเข้าสู่ระบบ</label>
                    <div class="col-lg-10">
                        <input name="username" type="username" class="form-control" value="<?php echo isset($_POST['username']) ? $_POST['username'] : ''; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword1" class="col-lg-2 control-label">รหัสผ่าน</label>
                    <div class="col-lg-10">
                        <input type="password" name="password" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label">อีเมล์</label>
                    <div class="col-lg-10">
                        <input type="email" name="email" class="form-control" value="<?php echo isset($_POST['email']) ? $_POST['email'] : ''; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputFullname" class="col-lg-2 control-label">ชื่อ-นามสกุลเต็ม</label>
                    <div class="col-lg-10">
                        <input type="fullname" name="fullname" class="form-control" value="<?php echo isset($_POST['fullname']) ? $_POST['fullname'] : ''; ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputEmail1" class="col-lg-2 control-label">รูปประจำตัว</label>
                    <div class="col-lg-10">
                        <input type="file" name="picture">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputStatus" class="col-lg-2 control-label">สถานะ</label>
                    <div class="col-lg-10">
                        <select name="status" class="form-control">
                            <option value="1">เปิดใช้งาน</option>
                            <option value="0">ปิดใช้งาน</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputLevel_id" class="col-lg-2 control-label">ระดับ</label>
                    <div class="col-lg-10">
                        <select name="level_id" class="form-control">
                            <option value="1">Admin</option>
                            <option value="2">Member</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                    </div>
                </div>

            </form>
  

        </div>
    </div>

<?php require('inc_footer.php'); ?>
