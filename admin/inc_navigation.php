<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">โปรเจ็กของฉัน</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">หน้าแรก</a></li>
                <?php if( isset($_SESSION['logged']) && $_SESSION['logged'] == 1){ ?>
                    <li><a href="member_view.php">สมาชิก</a></li>
                    <li><a href="product_view.php">สินค้า</a></li>
                    <li><a href="article_view.php">โพสต์</a></li>
                <?php } ?>
            </ul>

            <ul class="nav navbar-nav pull-right">
                <li class="dropdown">

                    <?php if( isset($_SESSION['logged']) ){ ?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo (isset($_SESSION['username'])?$_SESSION['username']:"");?> <b class="caret"></b></a>
                    <?php }else{?>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">ระบบสมาชิก <b class="caret"></b></a>
                    <?php } ?>

                    <ul class="dropdown-menu">
                        <?php if(isset($_SESSION['logged'])){ ?>

                        <li><a href="member_edit.php?id=<?php echo $_SESSION['id'];?>">แก้ไขข้อมูลส่วนตัว</a></li>
                        <li class="divider"></li>
                        <li><a href="sign_out.php">ออกจากระบบ</a></li>

                        <?php }else{ ?>

                        <li><a href="member_add.php">สมัครสมาชิก</a></li>
                        <li class="divider"></li>
                        <li><a href="sign_in.php">เข้าสู่ระบบ</a></li>

                        <?php } ?>
                    </ul>

                </li>
            </ul>

        </div><!--/.navbar-collapse -->
    </div>
</div>