<?php
require 'inc_header.php';

// เช็คค่าถ้าไม่มีการล๊อกอินเข้ามาให้...
if( $_SESSION['logged'] == 1 )
{
	// ถ้าล๊อกอินแล้วให้ไปหน้า dashboard
    echo '<script type="text/javascript">
                window.location="dashboard.php";
            </script>;';

}else{
	// ถ้ายังกลับไปที่หน้าล๊อกอิน(sign_in.php)
    echo '<script type="text/javascript">
                window.location="sign_in.php";
            </script>;';
}

?>