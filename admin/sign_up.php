<?php require('inc_header.php'); ?>

    <body>

        <?php require('inc_navigation.php'); ?>

        <div class="container">
            <div class="row">

                <br><br>
                <h1><span class="glyphicon glyphicon-user"></span>
                    Register
                </h1>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        Login Faill !
                </div>

						<form class="form-horizontal" role="form" action="register.php" method="post">

                          <div class="form-group">
                            <label for="inputName" class="col-lg-2 control-label">Name</label>
                            <div class="col-lg-10">
                              <input type="name" class="form-control" id="inputName" placeholder="Name">
                            </div>
                          </div>

						  <div class="form-group">
						    <label for="inputPassword1" class="col-lg-2 control-label">Password</label>
						    <div class="col-lg-10">
						      <input type="password" class="form-control" id="inputPassword1" placeholder="Password">
						    </div>
						  </div>

                          <div class="form-group">
                            <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                            <div class="col-lg-10">
                              <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                            </div>
                          </div>

						  <div class="form-group">
						    <div class="col-lg-offset-2 col-lg-10">
						      <button type="submit" class="btn btn-default">Sign Up</button>
						    </div>
						  </div>
						  
						</form>

            </div>
        </div>

    </body>

<?php require('inc_footer.php'); ?>
