<?php require('inc_header.php'); ?>

<body>

    <div class="container">
        <div class="row">

            <br><br>

            <?php
            // เช็คว่าถ้ายังไม่ได้ล๊อกอินมา ให้กลับไปล๊อกอินก่อน(sign_in.php)
            if( isset($_SESSION['logged']) && $_SESSION['logged'] == 1 )
            {
                // redirect to member_view.php
                echo '<script type="text/javascript">
                        window.location="dashboard.php";
                    </script>;';
            }
            // ถ้ามีการกด submit มาให้ทำตรงนี้
            if ( $_POST ) {
                // เก็บค่า username และ password ที่กรอกเข้ามา
                $username = isset($_POST['username']) ? $_POST['username'] : "";
                $password = isset($_POST['password']) ? $_POST['password'] : "";

                // เช็คว่าได้กรอกข้อมูลมาหรือไม่
                if ( $username == '' || $password == '' ) {
                    ?>

                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        กรุณากรอกข้อมูลในครบ
                    </div>

                <?php
                } else {
                    // ถ้ามีการกรอกข้อมูลมาครบให้มาทำตรงนี้
                    // นำค่า username และ password ไปเช็คในฐานข้อมูลว่ามีอยู่หรือไม่
                    $query = mysql_query("SELECT * FROM `accounts` WHERE username = '$username' && password = '$password' && status = '1' ");
                    $num = mysql_num_rows($query);

                    // ถ้าเจอข้อมูล
                    if ( $num > 0 ) {
                        
                        while ($row = mysql_fetch_array($query)) {
                            // set session
                            $_SESSION['id'] = $row['id'];
                            $_SESSION['username'] = $row['username'];
                            $_SESSION['status'] = $row['status'];
                            $_SESSION['logged'] = 1;
                            echo '<script type="text/javascript">
                                        window.location="dashboard.php";
                                    </script>;';
                        }

                    } else {
                        // ถ้าไม่เจอข้อมูล
                        ?>

                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง
                        </div>
                        <?php
                    }
                }
            }
            ?> 
            
            <div class="col-md-8 well" style="margin:0 160px;">
            <h1>
            <span class="glyphicon glyphicon-lock"></span>
                เข้าสู่ระบบ
            </h1>

            <!-- Start Form Login -->
            <form action="sign_in.php" class="form-horizontal" role="form" method="post">

                <div class="form-group">
                    <label for="inputUsername" class="col-lg-2 control-label">ชื่อผู้ใช้งาน</label>
                    <div class="col-lg-10">
                        <input type="text" name="username" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputPassword1" class="col-lg-2 control-label">รหัสผ่าน</label>
                    <div class="col-lg-10">
                        <input type="password" name="password" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button type="submit" class="btn btn-default">เข้าสู่ระบบ</button>
                    </div>
                </div>

            </form>
            <!-- End Form Login -->
            </div>

        </div>
    </div>

</body>

<?php require('inc_footer.php'); ?>
