<?php require('inc_header.php'); ?>
<body>

    <?php require('inc_navigation.php'); ?>

    <br><br>
    <div class="row well">

                
                <a href="member_add.php" class="btn btn-default"> <span class="glyphicon glyphicon-plus"></span> เพิ่มสมาชิก</a>
                <br><br>
                    
                <table class="table table-striped">
                    <thead>
                        <tr class="success">
                            <th>ID</th>
                            <th>ชื่อเข้าสู่ระบบ</th>
                            <th>อีเมล์</th>
                            <th>ชื่อ-นามสกุล</th>
                            <th>ตำแหน่ง</th>
                            <th>เพิ่มเมื่อ</th>
                            <th>แก้ไขเมื่อ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $sql  = "SELECT *  FROM `accounts` WHERE `status` = 1 AND `deleted` IS NULL";
                        $query = mysql_query($sql);

                        $Num_Rows = mysql_num_rows($query);

                        // โค๊ดแบ่งหน้า
                        $Per_Page = 5;   // per page

                        $Page = (isset($_GET["Page"]) ? $_GET['Page'] : 1);

                        $Prev_Page = $Page - 1;
                        $Next_Page = $Page + 1;

                        $Page_Start = (($Per_Page * $Page) - $Per_Page);

                        if ($Num_Rows <= $Per_Page) {
                            $Num_Pages = 1;
                        } else if (($Num_Rows % $Per_Page) == 0) {
                            $Num_Pages = ($Num_Rows / $Per_Page);
                        } else {
                            $Num_Pages = ($Num_Rows / $Per_Page) + 1;
                            $Num_Pages = (int) $Num_Pages;
                        }

                        $sql .= " ORDER BY id ASC LIMIT $Page_Start , $Per_Page";
                        $query = mysql_query($sql);

                        if (mysql_num_rows($query) > 0) {
                            // วนลูปข้อมูล
                            while ($item = mysql_fetch_array($query)) {
                                    ?>
                                    <tr>
                                        <td><?php echo $item['id']; ?>.</td>
                                        <td><?php echo $item['username']; ?></td>
                                        <td><?php echo $item['email']; ?></td>
                                        <td><?php echo $item['fullname']; ?></td>
                                        <td><?php
                                            if ($item['level_id'] == 1) {
                                                echo "admin";
                                            } else {
                                                echo "member";
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo date("Y", strtotime($item['created'])) + 543;
                                            echo "-";
                                            echo date("m-d", strtotime($item['created']));
                                            ?>
                                        </td>
                                        <td><?php echo date("Y-m-d", strtotime($item['deleted'])); ?></td>
                                        <td>

                                            <a href="member_edit.php?id=<?php echo $item['id']; ?>"><span class="glyphicon glyphicon-pencil"></span>
                                                แก้ไข
                                            </a> | 

                                            <a href="member_delete.php?id=<?php echo $item['id']; ?>" onclick="return confirm('ต้องการจะลบ !');"><span class="glyphicon glyphicon-trash"></span>
                                                ลบ
                                            </a>

                                        </td>
                                    </tr>
                                    <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>

                <div class="panel-footer">

                    <ul class="pagination">

                        <?php if ($Prev_Page): ?>
                            <li><a href="member_view.php?Page=<?= $Prev_Page; ?>">&laquo;</a></a></li>
                        <?php endif; ?>

                        <?php
                        for ($i = 1; $i <= $Num_Pages; $i++) {
                            if ($i != $Page) {
                                echo "<li><a href='$_SERVER[SCRIPT_NAME]?Page=$i'>$i</a> </li>";
                            } else {
                                echo "<li><a href='#'>$i</a></li>";
                            }
                        }
                        ?>

                            <?php if ($Page != $Num_Pages): ?>
                            <li><a href="member_view.php?Page=<?= $Next_Page; ?>">&raquo;</a>
<?php endif; ?>

                    </ul>

                </div>


        
    </div>

</body>

<?php require('inc_footer.php'); ?>
