<?php require('inc_header.php'); ?>

<body>

    <?php require('inc_navigation.php'); ?>

        <div class="row well">

            <div class="span10">
            <br><br>
            <h1><span class="glyphicon glyphicon-pencil"></span>
                เพิ่มโพสต์
            </h1>
            <?php
            // เช็คว่าถ้ายังไม่ได้ล๊อกอินมา ให้กลับไปล๊อกอินก่อน(sign_in.php)
            if( !isset($_SESSION['logged']) && $_SESSION['logged'] == '' )
            {
                echo '<script type="text/javascript">
                            window.location="dashboard.php";
                        </script>;';
            }

            // ถ้ามีการกด submit มาให้ทำตรงนี้
            if ($_POST) {
 
                // เช็คว่า ถ้าไม่ได้กรอกข้อมูลมาให้ทำตรงนี้
                if ( $_POST['name'] == ''  ||  $_POST['content'] == '' ) {
                    ?>
                    <div class="alert alert-warning">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        กรุณากรอกข้อูลให้ครบ !
                    </div>
                    <?php
                    
                } else {
                    // ถ้ามีการกรอกข้อมูลมาครบให้มาทำตรงนี้
                    // ประกาศตัว date_now เก็บค่าวันที่ เวลา ปัจจุบัน
                    $date_now = date("Y-m-d H:i:s");
                    
                    $result = mysql_query("INSERT INTO `posts` (`id`, `name`, `summary`, `content`, `status`, `picture`, `created`, `updated`, `deleted`, `account_id`) VALUES (NULL, '$_POST[name]', '$_POST[summary]', '$_POST[content]', '$_POST[status]', '', '$date_now', '$date_now', NULL, '$_SESSION[id]');");
                    
                    $last_id = mysql_insert_id();
                    
                    // ถ้ามีการอัพรูปภาพมาด้วย
                    if (isset($_FILES['picture']['name'])) {
                        
                        $file_type = $_FILES['picture']['type'];
                        // เช็คประเภทรูปภาพ
                        if ( $file_type == 'image/jpeg' || $file_type == 'image/png' || $file_type == 'image/gif') {
                            // เปลี่ยนชื่อ 
                            $Str_file = explode(".", $_FILES['picture']['name']);  // แยกชื่อไฟล์ด้วย "." ค่าที่ได้จะเป็น Array ครับ
                            $date = date('Y-m-d-H-i-s');
                            $new_name = $date . "." . $Str_file['1'];
                            // ย้ายรูปภาพไปอยู่ที่ Folder uploads ที่สร้างไว้แล้ว
                            if (move_uploaded_file($_FILES["picture"]["tmp_name"], "uploads/" . $new_name)) {
                                mysql_query("UPDATE `php_db`.`posts` SET `picture` = '$new_name' WHERE `posts`.`id` = '$last_id' ");
                            }
                        }
                    }
                    // redirect ไปหน้า article view
                    if ( isset($result) && $result == 1) {
                            echo '<script type="text/javascript">
                                        window.location="article_view.php";
                                    </script>;';
                    }
                    
                }
                ?>


                <?php
            }
            ?>


            <form action="article_add.php" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="inputName" class="col-lg-2 control-label">ชื่อเรื่อง</label>
                    <div class="col-lg-10">
                        <input name="name" type="text" class="form-control" value="<?php echo (isset($name)?$name:"")?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputSummary" class="col-lg-2 control-label">เรื่องย่อ</label>
                    <div class="col-lg-10">
                        <textarea name="summary"><?php echo (isset($summary)?$summary:"")?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputContent" class="col-lg-2 control-label">รายละเอียด</label>
                    <div class="col-lg-10">
                        <textarea name="content" rows="10"><?php echo (isset($content)?$contents:""); ?></textarea>
                    </div>
                </div>


                <div class="form-group">
                    <label for="inputPicture" class="col-lg-2 control-label">ภาพปก</label>
                    <div class="col-lg-10">
                        <input type="file" name="picture">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputStatus" class="col-lg-2 control-label">สถานะ</label>
                    <div class="col-lg-10">
                        <select name="status" class="form-control">
                            <option value="1">เปิดใช้งาน</option>
                            <option value="0">ปิดใช้งาน</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button type="submit" class="btn btn-success">ตกลง</button>
                    </div>
                </div>

            </form>
        </div>

        </div>

<?php require('inc_footer.php'); ?>
