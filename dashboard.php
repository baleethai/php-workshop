<?php require('header_inc.php'); ?>

    <body>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">My Admin</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">หน้าแรก</a></li>
                        <li><a href="#about">สมาชิก</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-header">Nav header</li>
                                <li><a href="#">Separated link</a></li>
                                <li><a href="#">One more separated link</a></li>
                            </ul>
                        </li>
                    </ul>

                    
                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Username <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <?php if(isset($isMemberLogin) && $isMemberLogin == 1): ?>
                                <li><a href="#">แก้ไขข้อมูลส่วนตัว</a></li>
                                <li class="divider"></li>
                                <li><a href="#">ออกจากระบบ</a></li>
                                <?php else: ?>
                                <li><a href="#">สมัครสมาชิก</a></li>
                                <li><a href="#">เข้าสู่ระบบ</a></li>
                                <li><a href="#">ลืมรหัสผ่าน</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    </ul>
                    
                </div><!--/.navbar-collapse -->
            </div>
        </div>


        <div class="container">>
            <div class="row">

                <br><br>
                <h1><span class="glyphicon glyphicon-user"></span>
                    member</h1>

                <table class="table table-striped" style="border:1px solid #ccc;">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php for($i=1;$i<30;$i++): ?>
                        <tr>
                            <td>1</td>
                            <td>Tum</td>
                            <td>Tum@gmail.com</td>
                            <td>129-234</td>
                            <td>
                                <a href="" class="btn btn-default"><span class="glyphicon glyphicon-pencil"></span>
                                    แก้ไข</a> 
                                <a href="" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span> ลบ</a>
                            </td>
                        </tr>
                        <?php endfor; ?>
                    </tbody>
                </table>

                <ul class="pagination">
                  <li><a href="#">&laquo;</a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li><a href="#">&raquo;</a></li>
                </ul>

                
            </div>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="bootstrap/js/jquery.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>

<?php require('footer_inc.php'); ?>